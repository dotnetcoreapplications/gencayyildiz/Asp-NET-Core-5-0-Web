# Özel Ders Formatında A’dan Z’ye Asp.NET Core 5.0 Web Programlama Eğitimi

***
# [Bu notları Gençay Yıldız hocamın sayesinde izlediğim bu playlistten öğrendim.](https://www.youtube.com/playlist?list=PLQVXoXFVVtp33KHoTkWklAo72l5bcjPVL)  Herkesin izlemesini tavsiye ederim. Mükemmel bir anlatım. Böyle mükemmel bir içerikli [Türkçe kaynağı](https://www.youtube.com/c/Gen%C3%A7ayY%C4%B1ld%C4%B1z) hiçbir yerde bulamazsınız.
***

- ## [BAŞLARKEN](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/1-BA%C5%9ELARKEN/ReadMe.md)
- ## [WEB MANTIĞI](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/2-WEB%20MANTI%C4%9EI/ReadMe.md)
- ## [HTTP PROTOKOLÜ](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/3-HTTP%20PROTOK%C3%9C/ReadMe.md)
- ## [SUNUCU ÇEŞİTLERİ](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/4-SUNUCU%20%C3%87E%C5%9E%C4%B0TLER%C4%B0/ReadMe.md)
- ## [WEB KAVRAMLARI](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/5-%20WEB%20KAVRAMLARI/ReadMe.md)
- ## [WEB UYGULAMASI GELİŞTİRME YAKLAŞIMLARI](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/6-%20WEB%20UYGULAMASI%20GEL%C4%B0%C5%9ET%C4%B0RME%20YAKLA%C5%9EIMLARI/ReadMe.md)
- ## [ASP.NET CORE PROJE OLUŞTURMA VE DOSYA YAPISI](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/7-ASP.NET%20CORE%20PROJE%20OLU%C5%9ETURMA%20VE%20DOSYA%20YAPISI/ReadMe.md)
- ## [MVC(MODEL – VİEW – CONTROLLER)](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/8-MVC(MODEL%20%E2%80%93%20V%C4%B0EW%20%E2%80%93%20CONTROLLER)/ReadMe.md)
- ## [CONTROLLER](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/9-CONTROLLER/ReadMe.md)
- ## [VİEW](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/10-V%C4%B0EW/ReadMe.md)
- ## [RAZOR](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/11-RAZOR/ReadMe.md)
- ## [HELPERS](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/12-HELPERS/ReadMe.md)
- ## [BINDINGS](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/13-B%C4%B0ND%C4%B0NGS/ReadMe.md)
- ## [KULLANICIDAN VERİ ALMA YÖNTEMLERİ](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/14-KULLANICIDAN%20VER%C4%B0%20ALMA%20Y%C3%96NTEMLER%C4%B0/ReadMe.md)
- ## [VALİDATİON(VERİ DOĞRULAMA)](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/15-VAL%C4%B0DAT%C4%B0ON(VER%C4%B0%20DO%C4%9ERULAMA)/ReadMe.md)
- ## [LAYOUT YAPILANMASI](https://gitlab.com/dotnetcoreapplications/gencayyildiz/Asp-NET-Core-5-0-Web/-/blob/main/16-LAYOUT%20YAPILANMASI/ReadMe.md)

﻿namespace OrnekUygulama.Models.ViewModels
{
    public class UserProduct
    {
        public User User { get; set; }
        public Product Product { get; set; }
    }
}
